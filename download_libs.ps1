$versions = @(
 ("3.28.0"),
 ("3.27.1"),
 ("3.27.0"),
 ("3.26.0"),
 ("3.25.2"),
 ("3.25.1"),
 ("3.25.0"),
 ("3.24.1"),
 ("3.24.0"),
 ("3.23.3"),
 ("3.23.2"),
 ("3.23.1"),
 ("3.23.0"),
 ("3.22.0"),
 ("3.21.5"),
 ("3.21.4"),
 ("3.21.3"),
 ("3.21.2"),
 ("3.21.1"),
 ("3.21.0"),
 ("3.20.9"),
 ("3.20.8"),
 ("3.20.7"),
 ("3.20.6"),
 ("3.20.5"),
 ("3.20.4"),
 ("3.20.3"),
 ("3.20.2"),
 ("3.20.1"),
 ("3.21.0"),
 ("3.19.5"),
 ("3.19.4"),
 ("3.19.3"),
 ("3.19.2"),
 ("3.19.1"), 
 ("3.19.0"),
 ("3.18.2"),
 ("3.18.1"), 
 ("3.18.0"),
 ("3.17.3"),
 ("3.17.2"),
 ("3.17.1")
)

foreach ($version in $versions) {
    $linuxUrl = "https://dl.cryptlex.com/downloads/v$version/LexActivator-Static-Linux.zip"
    $windowsUrl = "https://dl.cryptlex.com/downloads/v$version/LexActivator-Static-Win-VC14.zip"
    
    # Download the files if not present
    if (-not (Test-Path "lexactivator-$version-static-release-linux.zip")) {
        curl -L $linuxUrl --output lexactivator-$version-static-release-linux.zip &
    }
    if (-not (Test-Path "lexactivator-$version-static-release-win64.zip")) {
        curl -L $windowsUrl --output lexactivator-$version-static-release-win64.zip &
    }
}